from django.shortcuts import render

# Create your views here.

# -*- encoding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.contrib.auth import authenticate
from django.http import HttpResponseRedirect, HttpResponse,\
                        HttpResponseNotAllowed, HttpResponseBadRequest, \
                        HttpResponseForbidden
        
from django.core.mail import send_mail
from django.conf import settings

def index(request):
    template_name = 'index.html'
    return render_to_response(
		template_name, 
		{}, 
		context_instance=RequestContext(request)
	)
